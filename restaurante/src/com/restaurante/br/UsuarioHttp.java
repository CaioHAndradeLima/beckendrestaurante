package com.restaurante.br;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

import objetos.Usuario;

@Path("usuario")
public class UsuarioHttp {

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String inserir(String json) throws SQLException {
		Gson g = new Gson();
		
		Usuario usuario = g.fromJson(json, Usuario.class);
		usuario.inserir(true);
		
		return g.toJson(usuario);
	}
	
	/**********
	 * Recebe Usuario e senha e autentica no sistema
	 * retorna o id e perm do usuario
	 * se perm voltar como -1 acesso negado
	 * se perm voltar como -2 mais de 1 nome de login no bd
	 */
	@POST
	@Path("login")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public String verificar(String json) throws SQLException {
		Gson g = new Gson();
		
		Usuario usuario = g.fromJson(json, Usuario.class);
		usuario.verifyLogin();
		
		return g.toJson(usuario);
	}
	
}
