package com.restaurante.br;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import objetos.Payment;
import objetos.PaymentPedido;

@Path("payment")
public class PaymentHttp {

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserir(String json) throws SQLException {
		Gson g = new Gson();
		Payment payment = g.fromJson(json, Payment.class);
		//em esta esta o id do pedido
		PaymentPedido pay = new PaymentPedido();
		
		pay.setIdpedido(payment.getEstado());
		payment.setEstado(1);
		payment.inserir(true);
		
		pay.setIdpayment(payment.getId());
		pay.inserir(false);
		
		return Response.ok(g.toJson(payment)).build();
	}
	
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response alterar(String json) throws SQLException {
		Gson g = new Gson();
		Payment payment = g.fromJson(json, Payment.class);
		
		payment.alterar();
		
		
		return Response.ok(g.toJson(payment)).build();
	}
	
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response delete(String json) {
		Gson g = new Gson();
		Payment payment = g.fromJson(json, Payment.class);
		payment.deletar();
		return Response.ok(g.toJson(payment)).build();
	}
	
}
