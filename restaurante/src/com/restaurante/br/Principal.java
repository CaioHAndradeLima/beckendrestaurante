package com.restaurante.br;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET; //import da biblioteca jersey
import javax.ws.rs.POST;
import javax.ws.rs.Path; //import da biblioteca jersey
import javax.ws.rs.Produces; //import da biblioteca jersey
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.sun.jersey.core.header.FormDataContentDisposition;

import com.sun.jersey.multipart.FormDataParam;

@Path("/restaurante") // o @path define a URI do recurso que nesse caso ser� /helloworld
public class Principal {

	@GET // utilizando apenas o verbo GET, ou seja, vou apenas ler o recurso
	@Produces("text/plain") // define qual tipo MIME � retornado para o cliente
	public String exibir(){
		return "Hello World";
	}
	
	

} 
