package com.restaurante.br;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import objetos.Action;
import objetos.Pedido;

@Path("pedido")
public class PedidoHttp {

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pedidos(String json) throws SQLException {
		Gson g = new Gson();
		Pedido pedido = g.fromJson(json, Pedido.class);
		pedido.setEstado(1);
		pedido.inserir(true);

		return Response.ok(g.toJson(pedido)).build();
	}

	@GET
	@Path("{ultimoid}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getList(@PathParam("ultimoid") String ultimoid) {

		List<Pedido> lista = new Pedido().buscar(" where id < " + ultimoid, " order by id desc ", "50");

		return Response.ok(new Gson().toJson(lista)).build();
	}

	@GET
	@Path("search/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getAllPedido(@PathParam("id") String id) {

		Pedido pedido = new Pedido().search(id);

		return Response.ok(new Gson().toJson(pedido)).build();
	}
	
	@POST
	@Path("finalizar")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pedidoFinalizar(String json) throws SQLException {
		Gson g = new Gson();
		Pedido pedido = g.fromJson(json, Pedido.class);
		System.out.println("pedido finalizar " + pedido.getId());
		pedido.finalizar();

		return Response.ok(g.toJson(pedido)).build();
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response pedidoAlterar(String json) throws SQLException {
		Gson g = new Gson();
		Integer[] params = g.fromJson(json, Integer[].class);
		// param 1 id do pedido
		// param 2 id do produto
		// param 3 � para modificar em quantos la no servidor?

		System.out.println("idpedido " + params[0]);
		System.out.println("id do produto " + params[1]);
		System.out.println("dif of srrver " + params[2]);
		synchronized (PedidoHttp.this) {

			Pedido pedido = new Pedido().buscar(" where id = " + params[0], "");

			pedido.search();
			pedido.notifyProductQuantChanged(params[1], params[2]);

			return Response.ok(g.toJson(new Pedido())).build();
		}
	}

}
