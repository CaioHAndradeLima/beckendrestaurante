package com.restaurante.br;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.google.gson.Gson;

import objetos.Action;
import objetos.Produto;
import objetos.Usuario;

@Path("produto/")
public class ProdutoHttp {

	
	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response inserir(String json) throws SQLException {
		Gson g = new Gson();
		
		Produto produto = g.fromJson(json, Produto.class);
		produto.inserir(false);
		return Response.ok(g.toJson(produto), MediaType.APPLICATION_JSON).build();
	}
	
	
	
	/*Chamado quando um produto � procurando
	 * 
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("{nome}")
	public Response getProdutos(@PathParam("nome") String name) {
		/*
		List<Produto> produtos = new Produto().buscar("where nome LIKE '%" + name + "%'", " order by id desc", " limit 200");
		if(produtos == null) 
			produtos = new ArrayList<>();
		return Response.ok(new Gson().toJson(produtos), MediaType.APPLICATION_JSON).build();
		*/
		
		Produto pt = new Produto();
		Action ac = new Action();
		ac.setDat("data");
		ac.setObject(pt);
		return Response.ok(new Gson().toJson(ac)).build();
	}
	
	
	/*
	 * Chamado para listar todos os produtos
	 */
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("list/{idultimo}")
	public Response getProdutosList(@PathParam("idultimo") String idultimo) {
		
		List<Produto> produtos;
		if(idultimo.equals("0")) {
			produtos = new Produto().buscar("", " order by id desc" , " limit 30");
		} else
		produtos = new Produto().buscar("where id < " + idultimo, " order by id desc" , " limit 30");
		if(produtos == null) 
			produtos = new ArrayList<>();
		
		return Response.ok(new Gson().toJson(produtos), MediaType.TEXT_HTML).build();
	}
	
	
	@PUT
	@Produces(MediaType.APPLICATION_JSON)
	public Response alterar(String json) throws SQLException {
		Gson g = new Gson();
		Produto pt = g.fromJson(json, Produto.class);
		System.out.println("bug " + pt.getQuantidade());
		pt.alterar();
		return Response.ok(json).build();
	}
}
