package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class Payment {

	private int id, valor, estado;
	private String tipo;

	// estados possiveis:
	// 0 quando ainda nao foi salvo na base de dados
	// 1 quando esta inserido na base de dados
	// 2 quando foi cancelado

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getValor() {
		return valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public boolean inserir(boolean needRecuperarID) throws SQLException {

		boolean retorno = false;
		// cria um preparedStatement
		try ( // conectando
				Connection con = new Conexao().getConnection()) {
			// cria um preparedStatement
			String sql = "insert into payment" + " (valor,estado,tipo)" + " values (?,?,?) returning id;";
			// preenche os valores
			try (PreparedStatement stmt = con.prepareStatement(sql)) {
				// preenche os valores
				stmt.setInt(1, this.getValor());
				stmt.setInt(2, this.getEstado());
				stmt.setString(3, this.getTipo());
				// executa
				ResultSet rs = stmt.executeQuery();

				if (needRecuperarID)
					if(rs.next())
						setId( rs.getInt("id"));

				retorno = true;
				stmt.close();

			} finally {
				con.close();
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return retorno;
	}

	public void buscarUltimoId(Connection con) throws SQLException {
		setId(AuxiliarMethods.returnIdUltimoInBD(con, "payment"));
	}

	public void alterar() throws SQLException {

		StringUpdate update = new StringUpdate();
		update.createUpdate("pedido"); // Name Table
		update.addAtributeName("valor");
		update.addAtributeName("estado");
		update.addAtributeName("tipo");

		String sql = update.buildUpdate("id", Integer.toString(getId()), false); // Montando Clausula WHERE

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {
			stmt.setInt(1, getValor());
			stmt.setInt(2, getEstado());
			stmt.setString(3, getTipo());
			// Executa a instru��o
			stmt.executeUpdate();

			stmt.close();
		} finally {
			con.close();
		}
	}

	private void getDados(ResultSet rs) throws SQLException {
		this.setEstado(rs.getInt("estado"));
		this.setId(rs.getInt("id"));
		this.setTipo(rs.getString("tipo"));
		this.setValor(rs.getInt("valor"));
	}

	public List<Payment> buscar(String where) {
		List<Payment> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from payment " + where);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				lista = new ArrayList<>();
				Payment payment;
				do {
					payment = new Payment();
					payment.getDados(rs);
					lista.add(payment);

				} while (rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}

	public void notifyInsercaoOfPedido(Pedido pedido) throws SQLException {
		// TODO Auto-generated method stub
		if (getEstado() == 0) {
			setEstado(1);
			inserir(true);

			PaymentPedido paymentPedido = new PaymentPedido();
			paymentPedido.notifyInsercaoOfPedido(pedido, this);
		} else if (getEstado() == 2) {
			// se foi calcelado
			alterar();
		}
	}

	public void buscar() {
		// TODO Auto-generated method stub
		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from payment where id = " + id);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				do {
					getDados(rs);

				} while (rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

	public void deletar() {
		// TODO Auto-generated method stub
		try {
			// Create a statement
			Connection connection = new Conexao().getConnection();

			Statement stmt = connection.createStatement();

			// Prepare a statement to insert a record
			String sql = "DELETE FROM payment_pedido WHERE idpayment = " + getId();

			// Execute the delete statement
			int deleteCount = stmt.executeUpdate(sql);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
