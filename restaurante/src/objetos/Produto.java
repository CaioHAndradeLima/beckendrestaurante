package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class Produto {

	private int id, quantidade, preco_custo, preco_venda,estado, consumidos;
	
	private String nome;
	
	//estados possiveis:
	// 0 quando ainda nao foi salvo na base de dados
	// 1 quando esta inserido na base de dados
	// 2 quando foi cancelado de um pedido
	// 3 quando usuario deixou status como inativo

	public Produto(int idProduto) {
		// TODO Auto-generated constructor stub
		id = idProduto;
	}
	public Produto() {
		// TODO Auto-generated constructor stub
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getConsumidos() {
		return consumidos;
	}

	public void setConsumidos(int id) {
		this.consumidos = id;
	}
	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	public int getPreco_custo() {
		return preco_custo;
	}

	public void setPreco_custo(int preco_custo) {
		this.preco_custo = preco_custo;
	}

	public int getPreco_venda() {
		return preco_venda;
	}

	public void setPreco_venda(int preco_venda) {
		this.preco_venda = preco_venda;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public boolean inserir(boolean needRecuperarID) throws SQLException {
    	
    	boolean retorno = false;
    	// cria um preparedStatement
    	try ( // conectando
    			Connection con = new Conexao().getConnection()) {
    		// cria um preparedStatement
    		String sql = "insert into produto"
    				+ " (quantidade,preco_custo,preco_venda,estado,nome)"
    				+ " values (?,?,?,?,?) returning id;";
    		// preenche os valores
    		try (PreparedStatement stmt = con.prepareStatement(sql)) {
    			// preenche os valores
    			stmt.setInt(1, this.getQuantidade());
    			stmt.setInt(2, this.getPreco_custo());
    			stmt.setInt(3, this.getPreco_venda());
    			stmt.setInt(4, this.getEstado());
    			stmt.setString(5, this.getNome());
    			// executa
    			ResultSet rs = stmt.executeQuery();

    			if(rs.next())
    			         setId( rs.getInt("id") );

    			
    			retorno =  true;
    			stmt.close();
    		} finally {
    			con.close();
    		}

    	} catch (SQLException ex) {
    		ex.printStackTrace();
    	}
    	return retorno;
    }

	
	public void alterar() throws SQLException {

		StringUpdate update = new StringUpdate();
		update.createUpdate("produto"); // Name Table
		update.addAtributeName("quantidade");
		update.addAtributeName("preco_custo");
		update.addAtributeName("preco_venda");
		update.addAtributeName("estado");
		update.addAtributeName("nome");

		String sql = update.buildUpdate("id", Integer.toString(getId()), false); // Montando Clausula WHERE

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {

			stmt.setInt(1, getQuantidade());
			stmt.setInt(2, getPreco_custo());
			stmt.setInt(3, getPreco_venda());
			stmt.setInt(4, getEstado());
			stmt.setString(5, getNome());
			// Executa a instru��o
			stmt.executeUpdate();
			
			stmt.close();
		} finally {
			con.close();
		}
	}
	
	public void alterar(String nomeTable, int value) throws SQLException {

		StringUpdate update = new StringUpdate();
		update.createUpdate("produto"); // Name Table
		update.addAtributeName(nomeTable);

		String sql = update.buildUpdate("id", Integer.toString(getId()), false); // Montando Clausula WHERE

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {

			stmt.setInt(1, value);

			// Executa a instru��o
			stmt.executeUpdate();
			
			stmt.close();
		} finally {
			con.close();
		}
	}


	private void getDados(ResultSet rs) throws SQLException {
		this.setEstado(rs.getInt("estado"));
		this.setId(rs.getInt("id"));
		this.setNome(rs.getString("nome"));
		this.setPreco_custo(rs.getInt("preco_custo"));
		this.setPreco_venda(rs.getInt("preco_venda"));
		this.setQuantidade(rs.getInt("quantidade"));
	}
    
	public List<Produto> buscar(String where, String orderBy, String limit) {
		List<Produto> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from produto " + where + orderBy + limit);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			Produto produto;
			if (rs.next()) {
				lista = new ArrayList<>();

				do {
					produto = new Produto();
					produto.getDados(rs);

					lista.add(produto);
					// System.out.println(" \n" + est.getDescricaox());

				} while (rs.next());

			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}

	
	public void notifyInsercaoOfPedido(Pedido pedido) throws SQLException {
		// TODO Auto-generated method stub
		if(getEstado() == 0) {
			setEstado(1);
			inserir(true);
			
			ProdutoPedido produto = new ProdutoPedido();
			produto.notifyInsercaoOfPedido(pedido,this);
		} else if(getEstado() == 2) {
			//se foi calcelado
			alterar();
		}
	}

	public void buscar() {
		// TODO Auto-generated method stub
		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from produto where id = " + id );

			// executa um select
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {

				do {
					getDados(rs);

				} while (rs.next());

			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

}
