package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class ProdutoPedido {
	private int idproduto,idpedido,quantidade, id;

	public ProdutoPedido() {
		// TODO Auto-generated constructor stub
	}
	
	public ProdutoPedido(Produto pt, int idPedido) {
		// TODO Auto-generated constructor stub
		idproduto = pt.getId();
		quantidade = pt.getConsumidos();
		idpedido = idPedido;
	}


	public ProdutoPedido(Pedido pt, int idProduto, int quantidade) {
		// TODO Auto-generated constructor stub
		idpedido = pt.getId();
		this.quantidade = quantidade;
		idproduto = idProduto;
	}

	public int getIdproduto() {
		return idproduto;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	
	public void setIdproduto(int idproduto) {
		this.idproduto = idproduto;
	}

	public int getIdpedido() {
		return idpedido;
	}

	public void setIdpedido(int idpedido) {
		this.idpedido = idpedido;
	}

	public int getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	
    public boolean inserir(boolean needRecuperarID) throws SQLException {
    	
    	boolean retorno = false;
    	// cria um preparedStatement
    	try ( // conectando
    			Connection con = new Conexao().getConnection()) {
    		// cria um preparedStatement
    		String sql = "insert into produto_pedido"
    				+ " (idproduto,idpedido,quantidade)"
    				+ " values (?,?,?)";
    		// preenche os valores
    		try (PreparedStatement stmt = con.prepareStatement(sql)) {
    			// preenche os valores
    			stmt.setInt(1, this.getIdproduto());
    			stmt.setInt(2, this.getIdpedido());
    			stmt.setInt(3, this.getQuantidade());

    			// executa
    			stmt.execute();
    			
    			if(needRecuperarID)
    				buscarUltimoId(con);
    			
    			retorno = true;
    			stmt.close();

    		} finally {
    			con.close();
    		}

    	} catch (SQLException ex) {
    		ex.printStackTrace();
    	}
    	return retorno;
    }


    
    //idproduto,idpedido,quantidade, id
	private void getDados(ResultSet rs) throws SQLException {
		this.setIdproduto(rs.getInt("idproduto"));
		this.setId(rs.getInt("id"));
		this.setIdpedido(rs.getInt("idpedido"));
		this.setQuantidade(rs.getInt("quantidade"));
	}
    
	public List<ProdutoPedido> buscar(String where) {
		List<ProdutoPedido> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from produto_pedido " + where);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			
			if (rs.next()) {
				lista = new ArrayList<>();
				ProdutoPedido produtoP;
				do {
					produtoP = new ProdutoPedido();
					produtoP.getDados(rs);
					lista.add(produtoP);

				} while (rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}

	
    public void buscarUltimoId(Connection con) throws SQLException {
    	setId(AuxiliarMethods.returnIdUltimoInBD(con,"produto_pedido"));
    }

	public void notifyInsercaoOfPedido(Pedido pedido, Produto produto) throws SQLException {
		// TODO Auto-generated method stub
		setIdpedido(pedido.getId());
		setIdproduto(produto.getId());
		inserir(false);
	}
	
	
	public void alterar() throws SQLException {

		StringUpdate createStringUpdate = new StringUpdate();
		createStringUpdate.createUpdate("produto_pedido"); // Name Table
		// createStringUpdate.addAtributeName("ClickCategory");
		createStringUpdate.addAtributeName("idproduto");
		createStringUpdate.addAtributeName("idpedido");
		createStringUpdate.addAtributeName("quantidade");
		String sql = createStringUpdate.buildUpdate(" where idpedido = " + idpedido + " and idproduto = " + idproduto); // Montando Clausula WHERE

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {

			stmt.setInt(1, this.getIdproduto());
			stmt.setInt(2, this.getIdpedido());
			stmt.setInt(3, getQuantidade());

			// Executa a instru��o
			stmt.executeUpdate();
		} finally {
			con.close();
		}

	}

    
}
