package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class Usuario {

	private int id,perm;
	private String nome, senha;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getPerm() {
		return perm;
	}
	public void setPerm(int perm) {
		this.perm = perm;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}

	
    public boolean inserir(boolean needRecuperarID) throws SQLException {
    	
    	boolean retorno = false;
    	// cria um preparedStatement
    	try ( // conectando
    			Connection con = new Conexao().getConnection()) {
    		// cria um preparedStatement
    		String sql = "insert into usuario"
    				+ " (perm,nome,senha)"
    				+ " values (?,?,?)";
    		// preenche os valores
    		try (PreparedStatement stmt = con.prepareStatement(sql)) {
    			// preenche os valores
    			stmt.setInt(1, this.getPerm());
    			stmt.setString(2, this.getNome());
    			stmt.setString(3, this.getSenha());
    			// executa
    			stmt.execute();
    			
    			if(needRecuperarID)
    				buscarUltimoId(con);
    			
    			retorno = true;
    			stmt.close();

    		} finally {
    			con.close();
    		}

    	} catch (SQLException ex) {
    		ex.printStackTrace();
    	}
    	return retorno;
    }


    public void buscarUltimoId(Connection con) throws SQLException {
    	setId(AuxiliarMethods.returnIdUltimoInBD(con,"usuario"));
    }

	private void getDados(ResultSet rs) throws SQLException {
		this.setPerm(rs.getInt("perm"));
		this.setId(rs.getInt("id"));
		this.setNome(rs.getString("nome"));
		this.setSenha(rs.getString("senha"));
	}
    
	public List<Usuario> buscar(String where) {
		List<Usuario> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from usuario " + where);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			
			if (rs.next()) {
				lista = new ArrayList<>();
				Usuario usuario;
				do {
					usuario = new Usuario();
					usuario.getDados(rs);
					lista.add(usuario);

				} while (rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}
	
	public void verifyLogin() {
		// TODO Auto-generated method stub
		List<Usuario> lista = this.buscar("where nome = '" + this.getNome() + "' ");
		if(lista == null) {
			perm = -1;
		} else if (lista.size() > 1) {
			lista.clear();
			perm = -2;
		} else if(lista.size() == 0) {
			perm = -1;
		} else {
			this.setId(lista.get(0).getId());
			this.setPerm(lista.get(0).getPerm());
			lista.clear();
		}
		
	}

	
}
