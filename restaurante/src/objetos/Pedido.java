package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class Pedido {
	private int id,estado,idcomanda ;

	private List<Payment> listPayment;
	private List<Produto> listProduto;
	
	
	public Pedido() {
		
	}
	public Pedido(int idPedido) {
		setId(idPedido);
	}
	public List<Payment> getListPayment() {
		return listPayment;
	}

	public void setListPayment(List<Payment> listPayment) {
		this.listPayment = listPayment;
	}

	public List<Produto> getListProduto() {
		return listProduto;
	}

	public void setListProduto(List<Produto> listProduto) {
		this.listProduto = listProduto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public int getIdcomanda() {
		return idcomanda;
	}

	public void setIdcomanda(int idcomanda) {
		this.idcomanda = idcomanda;
	}
	
	

	
    public boolean inserir(boolean needRecuperarID) throws SQLException {
    	boolean retorno = false;
    	// cria um preparedStatement
    	try ( // conectando
    			Connection con = new Conexao().getConnection()) {
    		// cria um preparedStatement
    		String sql = "insert into pedido"
    				+ " (estado,idcomanda)"
    				+ " values (?,?) returning id;";
    		// preenche os valores
    		try (PreparedStatement stmt = con.prepareStatement(sql)) {
    			// preenche os valores
    			stmt.setInt(1, this.getEstado());
    			stmt.setInt(2, this.getIdcomanda());
    			// executa
    			ResultSet rs = stmt.executeQuery();
    			
    			if(rs.next() && needRecuperarID)
    				setId( rs.getInt("id") );
    			
    			retorno =  true;
    			stmt.close();
    		} finally {
    			con.close();
    		}
        	notifyInsercao();
    	} catch (SQLException ex) {
    		ex.printStackTrace();
    	}

    	return retorno;
    }


    /*****
     *  Chamado toda vez depois da insercao
     *  vamos inserir outros objetos se necessario
     */
    private void notifyInsercao() throws SQLException {
    	

    	listPayment = new ArrayList<>();
    	listProduto = new ArrayList<>();
    	/*
    	for(Payment payment : listPayment) {
    		payment.notifyInsercaoOfPedido(this);
    	}
    	
    	for(Produto produto : listProduto) {
    		produto.notifyInsercaoOfPedido(this);
    	} */
		
	}

	public void buscarUltimoId(Connection con) throws SQLException {
    	setId(AuxiliarMethods.returnIdUltimoInBD(con,"pedido"));
    }
    
    
	public void alterar() throws SQLException {

		StringUpdate update = new StringUpdate();
		update.createUpdate("pedido"); // Name Table
		update.addAtributeName("estado");

		String sql = update.buildUpdate("id", Integer.toString(getId()), false); // Montando Clausula WHERE

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {

			stmt.setInt(1, getEstado());
			// Executa a instru��o
			stmt.executeUpdate();
			
			stmt.close();
		} finally {
			con.close();
		}
	}

	public void alterar(String sql, int valueUpdate) throws SQLException {

		Connection con = new Conexao().getConnection();

		try (PreparedStatement stmt = con.prepareStatement(sql)) {

			stmt.setInt(1, getEstado());
			// Executa a instru��o
			stmt.executeUpdate();
			
			stmt.close();
		} finally {
			con.close();
		}
	}

	
	private void getDados(ResultSet rs) throws SQLException {
		this.setEstado(rs.getInt("estado"));
		this.setId(rs.getInt("id"));
		this.setIdcomanda(rs.getInt("idcomanda"));
	}
    
	public List<Pedido> buscar(String where, String orderBy, String limit) {
		List<Pedido> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from pedido " + where + orderBy + limit);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			
			if (rs.next()) {
				lista = new ArrayList<>();
				Pedido pedido;
				do {
					pedido = new Pedido();
					pedido.getDados(rs);

					lista.add(pedido);
					// System.out.println(" \n" + est.getDescricaox());

				} while (rs.next());

			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}
	
	public Pedido buscar(String where, String orderBy) {

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from pedido " + where + orderBy );

			// executa um select
			ResultSet rs = stmt.executeQuery();

			
			if (rs.next()) {
				do {
					getDados(rs);

				} while (rs.next());

			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return this;
	}

	
	//chamado quando precisamos buscar pedidos e mostrar ao usuario
	/*************
	 * ** Script para buscar os dados das tabelas de pedido e pagamento
	 * buscando os ids necessarios para proximas buscar, se houverem
	 */
	public Pedido search() {
		SelectBuilder builder = new SelectBuilder();

		listPayment = new ArrayList<>();
		listProduto = new ArrayList<>();

		builder.buscar(new StringBuilder().append("select pedido.id,pedido.estado,pedido.idcomanda, ").append( 
				"		produto_pedido.id, produto_pedido.idproduto, produto_pedido.quantidade, ").append( 
				"        payment_pedido.id,payment_pedido.idpayment from pedido ").append(  
				"        full join payment_pedido on pedido.id = payment_pedido.idpedido ").append( 
				"        full join produto_pedido on pedido.id = produto_pedido.idpedido ").append( 
				"        where pedido.id  = ").append( getId() ).append(" order by pedido.id desc").toString()  
				, new SelectBuilder.OnSelect() {
					Payment pay;
					Produto produto;
					
					@Override
					public void getDados(ResultSet rs) throws SQLException {
						preencherProdutoAndPaySimple(rs);
						System.out.println("getDados");

					}
					
					@Override
					public void getFirstDado(ResultSet rs) throws SQLException {
						System.out.println("getFirstDado");

						preencherProdutoAndPaySimple(rs);
						Pedido.this.setId(rs.getInt(1));
						Pedido.this.setEstado(rs.getInt("estado"));
						Pedido.this.setIdcomanda(rs.getInt("idcomanda"));

					}

					boolean existsDadoProduto = true;
					boolean existsDadoPayment = true;
					private void preencherProdutoAndPaySimple(ResultSet rs) throws SQLException {
						// TODO Auto-generated method stub
						System.out.println("preencherProdutoAndPaySimple");
						int id = rs.getInt(4); 
						if(id != 0 && existsDadoProduto) {
							//se temos produto 
							produto = new Produto();
							produto.setId(rs.getInt(5));
							produto.setConsumidos(rs.getInt(6));
							
							for(Produto pt : listProduto) {
								if(pt.getId() == produto.getId() ) {
									existsDadoProduto = false;
									break;
								}
							}
							if(existsDadoProduto) {
								listProduto.add(produto);
							} else {
								existsDadoProduto = true;
							}
						}
						

						if( (id = rs.getInt(7)) != 0 && existsDadoPayment) {
							//se temos payment
							
							pay = new Payment();
							pay.setId(rs.getInt(8));
							
							System.out.println("payment id " + pay.getId());

							for(Payment pt : listPayment) {
								if(pt.getId() == pay.getId() ) {
									existsDadoPayment = false;
									break;
								}
							}
							if(existsDadoPayment) {
								listPayment.add(pay);
							} else {
								existsDadoPayment = true;
							}
						}
					}

				});

		for(int i = 0 ; i < listPayment.size() ; i ++) {
			System.out.println("id pay " + listPayment.get(i).getId());
			listPayment.get(i).buscar();
		}
		for(int i = 0 ; i < listProduto.size() ; i ++) {
			System.out.println("id produto " + listProduto.get(i).getId());
			listProduto.get(i).buscar();
		}
		
		return this;

		
	}
	public Pedido search(String idC) {

		//procurando pedido ativo
		this.buscar("where idcomanda = " + idC + " and estado = 1 ", " order by id desc");
		
		return search();

	}
	
	public void notifyProductQuantChanged(int idProduto, int modifiedIn) throws SQLException {
		// TODO Auto-generated method stub
        //param 2  id do produto
        //param 3  � para modificar em quantos la no servidor?
		boolean isProcessed = false;
		for(Produto pt : listProduto) {
			if(pt.getId() == idProduto) {
				//se exists esse produto temos que altera-lo
				pt.setConsumidos( pt.getConsumidos() + (modifiedIn) );
				pt.setQuantidade( pt.getQuantidade() - (modifiedIn) );
				new ProdutoPedido(pt,getId()).alterar();
				pt.alterar("quantidade",pt.getQuantidade());
				isProcessed = true;
				break;
			}
		}
		
		//if foi processado otimo
		if(isProcessed)
			return;
		
		new ProdutoPedido(this,idProduto, modifiedIn).inserir(true);
		Produto pt = new Produto(idProduto);
		pt.buscar();
		pt.setQuantidade(pt.getQuantidade() - (modifiedIn));
		pt.alterar("quantidade",pt.getQuantidade());

	}
	
	public StringUpdate getBasicUpdate() {
		StringUpdate up = new StringUpdate();
		up.createUpdate("pedido");
		return up;
	}
	public void finalizar() throws SQLException {
		// TODO Auto-generated method stub
		setEstado(2);
		StringUpdate up = getBasicUpdate();
		up.addAtributeName("estado");
		this.alterar(up.buildUpdate("id", "" + id, false),2);
		
	}

	public void ativar() throws SQLException {
		// TODO Auto-generated method stub
		setEstado(1);
		StringUpdate up = getBasicUpdate();
		up.addAtributeName("estado");
		this.alterar(up.buildUpdate("id", "" + id, false),1);
		
	}
	
	
}
