package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class PaymentPedido {

	private int id,idpayment,idpedido;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getIdpayment() {
		return idpayment;
	}

	public void setIdpayment(int idpayment) {
		this.idpayment = idpayment;
	}

	public int getIdpedido() {
		return idpedido;
	}

	public void setIdpedido(int idpedido) {
		this.idpedido = idpedido;
	}

	public boolean inserir(boolean needRecuperarID) throws SQLException {
    	
    	boolean retorno = false;
    	// cria um preparedStatement
    	try ( // conectando
    			Connection con = new Conexao().getConnection()) {
    		// cria um preparedStatement
    		String sql = "insert into payment_pedido"
    				+ " (idpayment,idpedido)"
    				+ " values (?,?)";
    		// preenche os valores
    		try (PreparedStatement stmt = con.prepareStatement(sql)) {
    			// preenche os valores
    			stmt.setInt(1, this.getIdpayment());
    			stmt.setInt(2, this.getIdpedido());

    			// executa
    			stmt.execute();
    			
    			if(needRecuperarID)
    				buscarUltimoId(con);
    			
    			retorno = true;
    			stmt.close();

    		} finally {
    			con.close();
    		}

    	} catch (SQLException ex) {
    		ex.printStackTrace();
    	}
    	return retorno;
    }


    public void buscarUltimoId(Connection con) throws SQLException {
    	setId(AuxiliarMethods.returnIdUltimoInBD(con,"payment_pedido"));
    }
	
    
	private void getDados(ResultSet rs) throws SQLException {
		this.setId(rs.getInt("id"));
		this.setIdpedido(rs.getInt("idpedido"));
		this.setIdpayment(rs.getInt("idpayment"));
	}
    
	public List<PaymentPedido> buscar(String where) {
		List<PaymentPedido> lista = null;

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement("select * from payment_pedido " + where);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			
			if (rs.next()) {
				lista = new ArrayList<>();
				PaymentPedido paymentP;
				do {
					paymentP = new PaymentPedido();
					paymentP.getDados(rs);
					lista.add(paymentP);

				} while (rs.next());
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return lista;
	}

	public void notifyInsercaoOfPedido(Pedido pedido, Payment payment) throws SQLException {
		// TODO Auto-generated method stub
		setIdpedido(pedido.getId());
		setIdpayment(payment.getId());
		inserir(false);
	}

	
}
