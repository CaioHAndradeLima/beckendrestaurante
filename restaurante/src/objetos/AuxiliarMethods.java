package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class AuxiliarMethods {

	
	@Deprecated
    public static int returnIdUltimoInBD(Connection con, String nameTable) throws SQLException {
    	Statement t =  (Statement) con.createStatement();
        int autoIncKeyFromFunc = -1;
        ResultSet rs = t.executeQuery("SELECT LAST_INSERT_ID() FROM " + nameTable);
        if (rs.next()) {
            autoIncKeyFromFunc = rs.getInt(1);
        }
    	t.close();
    	rs.close();
    	return autoIncKeyFromFunc;
    }
    
}
