package objetos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import conexao.Conexao;

public class SelectBuilder {

	public interface OnSelect {
		void getDados(ResultSet rs) throws SQLException;

		void getFirstDado(ResultSet rs) throws SQLException;
	}

	public void buscar(String select, OnSelect on) {

		try {
			Connection con = new Conexao().getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement(select);

			// executa um select
			ResultSet rs = stmt.executeQuery();

			if (rs.next()) {
				on.getFirstDado(rs);
				if (rs.next())
					do {
						on.getDados(rs);

					} while (rs.next());

			}
			rs.close();
			stmt.close();
			con.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
	}

}
