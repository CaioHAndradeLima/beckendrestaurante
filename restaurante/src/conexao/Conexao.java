package conexao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Conexao {

	public Connection getConnection() {
		try {

			String url = "jdbc:postgresql://localhost:5432/restaurante";
			String usuario = "postgres";
			String senha = "123456";

			Class.forName("org.postgresql.Driver");

			return DriverManager.getConnection(url, usuario, senha);

		} catch (SQLException | ClassNotFoundException e) {
			System.out.println("Ocorreu um erro ao tentar conectar-se a base de dados VERIFIQUE");
			e.printStackTrace();
		}
		return null;
	}

}
